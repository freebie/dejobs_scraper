import requests
import json
import time
import re
import schedule
import datetime
import sqlite3
from bs4 import BeautifulSoup


class Scraper:
    def __init__(self, pages, schedule_time, db):
        self.urls = ['https://dejobs.org/jobs/ajax/joblisting/?sort=date&num_items={0}&offset={0}'.format((15*pages)+15)]
        self.schedule_time = schedule_time
        self.db = db
        self.set_db_coonection()

        # self.get_source_data()
        self.schedule_runer()

    def schedule_runer(self):
        schedule.every().day.at(self.schedule_time).do(Scraper.get_source_data, self)
        while True:
            schedule.run_pending()

    def get_source_data(self):
        for url in self.urls:
            self.parse(requests.request('GET', url))

    def parse(self, data):
        session_result = dict()

        bs = BeautifulSoup(data.text, 'lxml')
        for li in bs.find_all('li', {'class': 'direct_joblisting'}):
            job_title = li.find('span', {'class': 'resultHeader'}).text
            job_url = 'https://dejobs.org' + li.find('a')['href']
            company_name = li.find('b', {'class': 'job-location-information'}).text
            location = li.find('span', {'class': 'hiringPlace'}).text.strip()
            country = li.find('span', {'class': 'hiringPlace'}).find('meta')['content'] \
                if li.find('span', {'class': 'hiringPlace'}).find('meta') else 'Empty'
            date = datetime.datetime.now().strftime('%m/%d/%Y')
            source_description = requests.request('GET', job_url)
            description_bs = BeautifulSoup(source_description.text, 'lxml')
            description = description_bs.find('div', {'id': 'direct_jobDescriptionText'}).text
            session_result[job_title] = {
                'job_title': job_title,
                'job_url': job_url,
                'company_name': company_name,
                'location': location,
                'country': country,
                'date': date,
                'description': description,
            }
        session_result = self.db_save(session_result)

        self.data_to_json(session_result)

    @staticmethod
    def data_to_json(data: dict):
        with open('session-{0}.json'.format(int(time.time())*1000), 'w') as outfile:
            json.dump(data, outfile)

    def set_db_coonection(self):
        conn = sqlite3.connect(self.db)
        c = conn.cursor()
        sql = 'create table if not exists ' + 'vacancy' + \
              ' ( job_title text, job_url text, company_name text, location text, country text, date text, description text ) '
        c.execute(sql)
        conn.commit()
        conn.close()

    def db_save(self, data):
        conn = sqlite3.connect(self.db)
        c = conn.cursor()
        result = dict()
        for key in data:
            vacancy = data[key]
            sql = "select * from vacancy where job_url=?"
            set = c.execute(sql, (vacancy['job_url'],))

            if not set.fetchall():
                insert = 'insert into vacancy values (?, ?, ?, ?, ?, ?, ?)'

                result[key] = data[key]
                c.execute(insert, (
                    vacancy['job_title'],
                    vacancy['job_url'],
                    vacancy['company_name'],
                    vacancy['location'],
                    vacancy['country'],
                    vacancy['date'],
                    vacancy['description'],
                ))

        conn.commit()
        conn.close()

        return result


if __name__ == '__main__':
    schedule_time = str()
    pages = None

    while not re.match(r'[0-9]{2}:[0-9]{2}', schedule_time):
        print('Enter time for check. Example: "11:55" or Enter to pass and set default time at 11:55')
        schedule_time = input()
        if len(schedule_time) == 0:
            schedule_time = '11:55'
            break

    while not pages:
        print('Enter number of page for check more than 0 and less than 50.')
        try:
            pages = int(input())
            if pages < 0 or pages > 50:
                pages = None
        except ValueError:
            print('Enter number please')

    Scraper(pages, schedule_time, 'dejobs.db')
